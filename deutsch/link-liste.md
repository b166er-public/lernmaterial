# Linkliste / список ссылок

**Viele Links aus dieser Zusammenfassung und noch viele Weitere können unter der folgenden Adresse vorgefunden werden** (Seite leider nur auf Deutsch):

**Многие ссылки из этого резюме и многое другое можно найти по следующему адресу** (страница, к сожалению, только на немецком языке):

https://deutsch-lernen.zum.de/wiki/Willkommen-Materialien_Ukraine#Bildw.C3.B6rterb.C3.BCcher_Deutsch-Ukrainisch

und/и

https://padlet.com/rsben/dukr


## Kategorie: Erste Schritte / Рубрика: Начало работы

---

Bildwörterbuch Ukrainisch-Deutsch

Украинско-немецкий словарь в картинках

http://fluechtlingshilfe-muenchen.de/wp-content/uploads/2021/10/Fluechtlingshilfe_Deutschheft_Ukrainisch.pdf

---

Ein paar Videos zur Einführung

Несколько ознакомительных видео

https://www.youtube.com/channel/UC4iCToL_j6ABU44o7wHHSrg/videos
https://www.youtube.com/playlist?list=PL2sWi-Ow64weyyRccn3H6Q89m23bJAt65

---

Klett - Willkommen bei uns - viele Phrasen

Klett - Добро пожаловать к нам - много фраз

https://klett-sprachen.pim.red/_files_media/downloads/9783126740203_WillkommenBeiUns_DL.pdf

---

Noch eine Sammlung von alltäglichen Phrasen

Очередной сборник повседневных фраз

https://www.buchstaben.com/download/E-Book.pdf

---

Folien zu diversen Themen - eher für Kinder

Слайды на разные темы - больше для детей

https://www.twinkl.de/resources/materialien-auf-deutsch-resources/das-twinkl-portal-fr-fremdsprachen-deutschland/deutsch-ukrainische-materialien-das-twinkl-portal-fur-fremdsprachen-deutschland

---


## Kategorie: Grammatik / Категория: Грамматика

---

Hueber – Grammatik

Хьюбер – грамматика

https://www.hueber.de/sixcms/media.php/36/7480-63_001_01_deutsch_kompakt.pdf

---

Deutsche Grammatik - ausführlich - ca. 400 Seiten

Немецкая грамматика - подробная - около 400 страниц

https://blogs.sch.gr/etsaroucha/files/2020/04/EasyDeutsch_Grammatik_V8.2-2.pdf

---

Deutsche Grammatik - eher für Fortgeschrittene

Немецкая грамматика - больше для продвинутых учащихся

https://www.paukert.at/sprachen/deugra.pdf

---

Langenscheidt - Deutsche Grammatik - kurz und schmerzlos

Langenscheidt - Немецкая грамматика - кратко и просто

https://u4ebagermania.ru/upload/iblock/618/618c50b3bcc4adc705080173b51ac9fe.pdf

---

